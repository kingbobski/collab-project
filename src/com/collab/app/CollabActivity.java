package com.collab.app;
 
import android.app.Activity;
import android.app.Notification;
import android.app.NotificationManager;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
 
public class CollabActivity extends Activity {
     
 private static String logtag = "Collab App";
   
 /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) 
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
         
         Button buttonStart = (Button)findViewById(R.id.buttonStart);       
         buttonStart.setOnClickListener(startListener);
         Button buttonStop = (Button)findViewById(R.id.buttonStop);       
         buttonStop.setOnClickListener(stopListener);
    }
     
    //Create an anonymous implementation of OnClickListener
    private OnClickListener startListener = new OnClickListener() 
    {
        public void onClick(View v) 
        {
//          setContentView(R.layout.main);

        }
    };
     

    private OnClickListener stopListener = new OnClickListener() 
    {
        public void onClick(View v) 
        {

        }
    };
     
     
   @Override
     protected void onStart() 
   {
        super.onStart(); 
    }
   
    @Override
    protected void onResume() 
    {
        super.onResume();
    }
    
    @Override
    protected void onPause() 
    { 
        super.onPause();
    }
    
    @Override
    protected void onStop() 
    { 
        super.onStop();
    }
    
    @Override
    protected void onDestroy()
    {

        super.onDestroy();
    }
}